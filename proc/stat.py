import numpy as np
import pandas as pd
import matplotlib as mp

data = pd.read_csv('../survey/merged_v1_v2.csv')
data = data.sort(['id'])
print '##### AGE #####\n', data['age'].mean()
print '##### GENDER #####\n', data['gender'].value_counts()
print '##### EDUCATION #####\n', data['education'].value_counts()
print '##### PLAYED #####\n', data['played'].value_counts()
print '##### DRUG #####\n', data['drug'].value_counts()
print '##### HAIR #####\n', data['hair'].value_counts()
print '##### PLAYFREQ #####\n', data['playfreq'].value_counts()
print '##### knowslender #####\n', data['knowslender'].value_counts()
print '##### GAME TYPES PLAYED #####\n', data[['play_fps','play_horror','play_sport','play_rts','play_simulation','play_rpg']].sum()