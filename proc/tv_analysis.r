#library(matrixStats)
#library(ez)
#library(reshape2)
#library(heplots)
#library(schoRsch)
#library(lattice)
#library(psych)

setwd("e:/CSIKI/BCI/fear_intensity_v2/proc/")
qdata <- read.csv("../survey/merged_v1.csv")
qdata <-qdata[order(qdata$id),]
names(qdata) <- gsub(names(qdata), pattern = "happyness", replacement = "happiness")
qdata$id <- factor(qdata$id)

base <- qdata[,grep(names(qdata), pattern = "base")]
day <- qdata[,grep(names(qdata), pattern = "day")]
night <- qdata[,grep(names(qdata), pattern = "night")]

plot(colMeans(base), type = "b", col = "black", lty = "dotted", xaxt = "n", xlab = NA, ylab = "Mean score (1-10)", ylim = c(1,10))
lines(colMeans(day), type = "b", col = "orange")
lines(colMeans(night), type = "b", col = "blue")
axis(1, at = 1:8, labels = gsub(names(base), pattern = "base_", replacement = ""))
grid()
legend("top",legend = c("Baseline","Day game","Night game"), lwd=1, lty = c("dotted","solid","solid"), col = c("black","orange","blue"))


# Differences between emotions

calmnessLong <- melt(cbind(id = qdata$id, subset(qdata, select = c(grep(names(qdata),pattern = "_calmness")))), "id")
calmnessANOVA <- ezANOVA(data = calmnessLong, dv = .(value), wid = .(id), within = .(variable), type = 3, detailed=TRUE)
anova_out(calmnessANOVA, sph.cor = "HF")
pairwise.t.test(calmnessLong$value, calmnessLong$variable, paired = T, p.adjust.method = "bonf" )
etasq(angerANOVA$ANOVA)

disgustLong <- melt(cbind(id = qdata$id, subset(qdata, select = c(grep(names(qdata),pattern = "_disgust")))), "id")
disgustANOVA <- ezANOVA(data = disgustLong, dv = .(value), wid = .(id), within = .(variable), type = 3, detailed=TRUE)
anova_out(disgustANOVA, sph.cor = "HF")
pairwise.t.test(disgustLong$value, disgustLong$variable, paired = T, p.adjust.method = "bonf" )

happinessLong <- melt(cbind(id = qdata$id, subset(qdata, select = c(grep(names(qdata),pattern = "_happiness")))), "id")
happinessANOVA <- ezANOVA(data = happinessLong, dv = .(value), wid = .(id), within = .(variable), type = 3, detailed=TRUE)
anova_out(happinessANOVA, sph.cor = "HF")
pairwise.t.test(happinessLong$value, happinessLong$variable, paired = T, p.adjust.method = "bonf" )

sadnessLong <- melt(cbind(id = qdata$id, subset(qdata, select = c(grep(names(qdata),pattern = "_sadness")))), "id")
sadnessANOVA <- ezANOVA(data = sadnessLong, dv = .(value), wid = .(id), within = .(variable), type = 3, detailed=TRUE)
anova_out(sadnessANOVA, sph.cor = "HF")
pairwise.t.test(sadnessLong$value, sadnessLong$variable, paired = T, p.adjust.method = "bonf" )

angerLong <- melt(cbind(id = qdata$id, subset(qdata, select = c(grep(names(qdata),pattern = "_anger")))), "id")
angerANOVA <- ezANOVA(data = angerLong, dv = .(value), wid = .(id), within = .(variable), type = 3, detailed=TRUE)
anova_out(angerANOVA, sph.cor = "HF")
pairwise.t.test(angerLong$value, angerLong$variable, paired = T, p.adjust.method = "bonf" )

fearLong <- melt(cbind(id = qdata$id, subset(qdata, select = c(grep(names(qdata),pattern = "_fear")))), "id")
fearANOVA <- ezANOVA(data = fearLong, dv = .(value), wid = .(id), within = .(variable), type = 3, detailed=TRUE)
anova_out(fearANOVA, sph.cor = "HF")
pairwise.t.test(fearLong$value, fearLong$variable, paired = T, p.adjust.method = "bonf" )

excitementLong <- melt(cbind(id = qdata$id, subset(qdata, select = c(grep(names(qdata),pattern = "_excitement")))), "id")
excitementANOVA <- ezANOVA(data = excitementLong, dv = .(value), wid = .(id), within = .(variable), type = 3, detailed=TRUE)
anova_out(excitementANOVA, sph.cor = "HF")
pairwise.t.test(excitementLong$value, excitementLong$variable, paired = T, p.adjust.method = "bonf" )

surpriseLong <- melt(cbind(id = qdata$id, subset(qdata, select = c(grep(names(qdata),pattern = "_surprise")))), "id")
surpriseANOVA <- ezANOVA(data = surpriseLong, dv = .(value), wid = .(id), within = .(variable), type = 3, detailed=TRUE)
anova_out(surpriseANOVA, sph.cor = "HF")
pairwise.t.test(surpriseLong$value, surpriseLong$variable, paired = T, p.adjust.method = "bonf" )

# No significant difference in disgust, sadness, and anger.
# The biggest fear was elicited by the night situation, apart from that, the other situations triggered the same low fear response. The same was true for surprise.
# 
# In calmness, happyness the same pattern emerged with significant differences between each emotion: highest: baseline, 2. day, 3. night game, 4. death. 




################ Physiology #################################


ansdata <- read.csv("c:/Users/Tamas/Documents/synetiq/theseses/viktor toth/epoched_merged_all.csv" )
scr <- subset(ansdata, select = grep(names(ansdata), pattern = "_SCR"))
scr$mean <- rowMeans(ansdata[,grep(names(ansdata), pattern = "_SCR")], na.rm = T)
scr <- cbind(scr, MediaName = ansdata$MediaName, epoch_rel = ansdata$epoch_rel)
scr <- subset(scr, MediaName %in% c("open", "night", "close", "day"))
scr$MediaName <- factor(scr$MediaName, levels = c("open", "close", "day", "night"), labels = c("Open-eye baseline", "Closed-eye baseline", "Day game","Night game"))
scr <- scr[order(scr$MediaName, scr$epoch_rel),]
xyplot(data = scr, mean ~ epoch_rel | MediaName, type = "l", ylab = "Standardized mean (z)", xlab = "Time (s)")

# tapply(scr$mean, factor(scr$MediaName), mean, na.rm = T)

scrLong <- melt(scr, id = c("epoch_rel", "MediaName"))
scrLong <- subset(scrLong, complete.cases(scrLong))

scrANOVA <- ezANOVA(data = scrLong, dv = .(value), wid = .(variable), within = .(MediaName), within_full = .( epoch_rel), type = 3, detailed=TRUE)
anova_out(scrANOVA, sph.cor = "HF")
scrPlot <- ezPlot(data = scrLong, dv = value, wid = variable, within = MediaName, within_full = epoch_rel, x = MediaName)
scrPlot + scale_x_discrete(name="", labels=c("Open-eye baseline", "Closed-eye baseline", "Day game","Night game")) +
        scale_y_continuous(name="Standardized mean (z)")

scl <- subset(ansdata, select = grep(names(ansdata), pattern = "_SCL"))
scl$mean <- rowMeans(ansdata[,grep(names(ansdata), pattern = "_SCL")], na.rm = T)
scl <- cbind(scl, MediaName = ansdata$MediaName, epoch_rel = ansdata$epoch_rel)
scl <- subset(scl, MediaName %in% c("open", "night", "close", "day"))
scl$MediaName <- factor(scl$MediaName, levels = c("open", "close", "day", "night"), labels = c("Open-eye baseline", "Closed-eye baseline", "Day game","Night game"))
scl <- scl[order(scl$MediaName, scl$epoch_rel),]
xyplot(data = scl, mean ~ epoch_rel | MediaName, type = "l", ylab = "Standardized mean (z)", xlab = "Time (s)",
       
       
       )
# tapply(scl$mean, factor(scl$MediaName), mean, na.rm = T)

sclLong <- melt(scl, id = c("epoch_rel", "MediaName"))
sclLong <- subset(sclLong, complete.cases(sclLong))

sclANOVA <- ezANOVA(data = sclLong, dv = .(value), wid = .(variable), within = .(MediaName), within_full = .( epoch_rel), type = 3, detailed=TRUE)
anova_out(sclANOVA, sph.cor = "HF")

sclPlot <- ezPlot(data = sclLong, dv = value, wid = variable, within = MediaName, within_full = epoch_rel, x = MediaName, type = 3)
sclPlot + scale_x_discrete(name="", labels=c("Open-eye baseline", "Closed-eye baseline", "Day game","Night game")) +
        scale_y_continuous(name="Standardized mean (z)")


describeBy(sclLong$value, factor(sclLong$MediaName))
pairwise.t.test(sclLong$value, factor(sclLong$MediaName), paired = T)


sclANOVA <- ezANOVA(data = sclLong, dv = .(value), wid = .(variable), within = .(MediaName), within_full = .(MediaName), type = 3, detailed=TRUE)
anova_out(sclANOVA, sph.cor = "HF")


