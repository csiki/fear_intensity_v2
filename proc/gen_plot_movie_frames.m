function gen_plot_movie_frames(subj_id, feature_mx)

    addpath(genpath('e:\CSIKI\BCI\fear_intensity_v2\proc\export_fig\'));
    figure;
    axis tight
    
    % indices
    fea = 3;
    hr  = 4;
    gsr = 5;
    
    % limits
    time_window = 21; % in sec (5-5 secs to left and right)
    window_granularity = 0.25; % TODO
    ylim_fea = [min(feature_mx(:, fea)), max(feature_mx(:, fea))];
    ylim_hr  = [min(feature_mx(:, hr)), max(feature_mx(:, hr))];
    ylim_gsr = [min(feature_mx(:, gsr)), max(feature_mx(:, gsr))];
    
    vertical_y_fea = linspace(ylim_fea(1), ylim_fea(2), 10);
    vertical_y_hr  = linspace(ylim_hr(1), ylim_hr(2), 10);
    vertical_y_gsr = linspace(ylim_gsr(1), ylim_gsr(2), 10);
    
    fea_plot_bg = imread('../show/plot_frames/eeg_plot_bg.jpg');
    hr_plot_bg  = imread('../show/plot_frames/hr_plot_bg.jpg');
    gsr_plot_bg = imread('../show/plot_frames/gsr_plot_bg.jpg');
    
    % gen frames
    mkdir(sprintf('../show/plot_frames/%d', subj_id));
	for t = 0 : window_granularity : length(feature_mx)
        xlim_time = [t - time_window, t + time_window]; % put the current time in the middle
        vertical_x = zeros(10, 1) + t;
        image_id = t * (1 / window_granularity) + 1;
        %% fea frame
        % img bg
        xlen = time_window / 1.5;
        ximg = linspace(t-1 + time_window - xlen, t-1 + time_window, size(fea_plot_bg, 2));
        ylen = (max(feature_mx(:, fea)) - min(feature_mx(:, fea))) / 362 * (xlen * 465 / (2*time_window)) * size(fea_plot_bg, 1) / size(fea_plot_bg, 2);
        yimg = linspace(min(feature_mx(:, fea)) + ylen, min(feature_mx(:, fea)), size(fea_plot_bg, 1));
        image(ximg, yimg, fea_plot_bg, 'CDataMapping', 'scaled'), set(gca,'YDir','normal'), hold on;
        % plot
        plot(feature_mx(:,1), feature_mx(:, fea), 'b', 'LineWidth', 3), hold on;
        plot(vertical_x, vertical_y_fea, 'k', 'LineWidth', 2), hold off;
        xlim(xlim_time), ylim(ylim_fea);
        leg = legend('FEA');
        set(leg, 'FontSize', 14);
        set(gcf, 'Color', 'none'), set(gca, 'Color', 'w', 'XTick', [], 'YTick', [], 'Linewidth', 2);
        export_fig(sprintf('../show/plot_frames/%d/fea%d.png', subj_id, image_id), '-transparent');
        %% hr frame
        % img bg
        xlen = time_window / 3;
        ximg = linspace(t + time_window - xlen - 1, t + time_window - 1, size(hr_plot_bg, 2));
        ylen = (max(feature_mx(:, hr)) - min(feature_mx(:, hr))) / 362 * (xlen * 465 / (2*time_window)) * size(hr_plot_bg, 1) / size(hr_plot_bg, 2);
        yimg = linspace(min(feature_mx(:, hr)) + ylen, min(feature_mx(:, hr)), size(hr_plot_bg, 1));
        image(ximg, yimg, hr_plot_bg, 'CDataMapping', 'scaled'), set(gca,'YDir','normal'), hold on;
        % plot
        plot(feature_mx(:,1), feature_mx(:, hr), 'g', 'LineWidth', 3), hold on;
        plot(vertical_x, vertical_y_hr, 'k', 'LineWidth', 2), hold off;
        xlim(xlim_time), ylim(ylim_hr);
        leg = legend('HR');
        set(leg, 'FontSize', 14);
        set(gcf, 'Color','none'), set(gca, 'Color', 'w', 'XTick', [], 'YTick', [], 'Linewidth', 2);
        export_fig(sprintf('../show/plot_frames/%d/hr%d.png', subj_id, image_id), '-transparent');
        %% gsr frame
        % img bg
        xlen = time_window / 2;
        ximg = linspace(t + time_window - xlen - 0.5, t + time_window - 0.5, size(gsr_plot_bg, 2));
        ylen = (max(feature_mx(:, gsr)) - min(feature_mx(:, gsr))) / 362 * (xlen * 465 / (2*time_window)) * size(gsr_plot_bg, 1) / size(gsr_plot_bg, 2);
        yimg = linspace(min(feature_mx(:, gsr)) + ylen + ylen*0.1, min(feature_mx(:, gsr)) + ylen*0.1, size(gsr_plot_bg, 1));
        image(ximg, yimg, gsr_plot_bg, 'CDataMapping', 'scaled'), set(gca,'YDir','normal'), hold on;
        % plot
        plot(feature_mx(:,1), feature_mx(:, gsr), 'r', 'LineWidth', 3), hold on;
        plot(vertical_x, vertical_y_gsr, 'k', 'LineWidth', 2), hold off;
        xlim(xlim_time), ylim(ylim_gsr);
        leg = legend('GSR');
        set(leg, 'FontSize', 14);
        set(gcf, 'Color','none'), set(gca, 'Color', 'w', 'XTick', [], 'YTick', [], 'Linewidth', 2);
        export_fig(sprintf('../show/plot_frames/%d/gsr%d.png', subj_id, image_id), '-transparent');
	end
    close(gcf)

end