% fill SETTING_VARIATIONS array with different settings for the main script
SETTING_VARIATIONS = [];
DEF_SETTINGS = struct;

%% default settings
DEF_SETTINGS.name = 'original';
DEF_SETTINGS.use_gsr_valid_subj_list = 1;
DEF_SETTINGS.hr_or_gsr_regulation_feature_index = 5; % 4 HR, 5 GSR
DEF_SETTINGS.window_len = 1; % in sec; window of frequency calculation
DEF_SETTINGS.regulation_window_len = 5; % time between fea moments and after fea moments
DEF_SETTINGS.regulation_hr_gsr_reaction_time = 1; % in sec; 1-3 in literature (WIKI)
DEF_SETTINGS.regulation_avg_hr_gsr_win_len = 4; % in sec, length of averaging over at and after fea peak
DEF_SETTINGS.fea_variance_threshold_multiplier = 5;
DEF_SETTINGS.fea_left_electrode = 3; % F3 (index number)
DEF_SETTINGS.fea_right_electrode = 12; % F4 (index number)
DEF_SETTINGS.fea_freqs = 8:0.5:13; % frequencies for FEA calculation (average over these)
SETTING_VARIATIONS = [SETTING_VARIATIONS, DEF_SETTINGS];

%% AF3-AF4 testing
AF3_AF4_SETTINGS = DEF_SETTINGS; % base is the same
AF3_AF4_SETTINGS.name = 'af3-af4_def';
AF3_AF4_SETTINGS.fea_left_electrode = 1;
AF3_AF4_SETTINGS.fea_right_electrode = 14;