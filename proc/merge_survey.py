import pandas as pd
import numpy as np
import pandas as pd
import os

#return # under control

def yesnotrans(ynstr):
    if ynstr == ynstr:
        return yesnodict[ynstr]
    return 0

path = '../survey/'
csvFileNames = ['base.csv', 'day.csv', 'night.csv']
csvPrefixes = ['base_', 'day_', 'night_']
emoNames = ['calmness', 'disgust', 'happiness', 'sadness', 'anger', 'fear', 'excitement', 'surprise']
schooldict = {'ISK1': 'elementary', 'ISK2': 'secondary', 'ISK3': 'high'}
handdict = {'KEZ1': 'R', 'KEZ2': 'L'}
slenderdict = {'S1': 'none', 'S2': 'heard', 'S3': 'video', 'S4': 'played'}
playfreqdic = {'GYAK1': 'annually', 'GYAK2': 'raremonthly', 'GYAK3': 'monthly', 'GYAK4': 'weekly', 'GYAK5': 'raredaily', 'GYAK6': 'daily'}
hairdict = {'HAJ1': 'bald', 'HAJ2': 'short', 'HAJ3': 'necklen', 'HAJ4': 'shoulderlen', 'HAJ5': 'long'}
yesnodict = {'Y': 1, 'N': 0}

# id mapping
#subjectIdArray = np.loadtxt('../subject_id_mapping.txt', skiprows = 1, dtype = int)
#subjectIdMap = dict()
#for a in subjectIdArray:
#    subjectIdMap[a[1]] = a[0]

# load csv
DFs = dict()
for csv in csvFileNames:
    DFs[csv] = pd.read_csv(path + csv)
dfFilter = pd.read_csv(path + 'filter.csv')

# fill cols
cols = ['id','age','gender','education','hand','headinj','treatment','chronic','drug','hair','played','playfreq',
        'play_fps','play_horror','play_sport','play_rts','play_simulation','play_rpg', 'knowslender']
for pref in csvPrefixes:
    extracols = [pref + emo for emo in emoNames]
    cols += extracols

# fill merged dataframe
rec_dirs = os.listdir('../rec/')
merged = pd.DataFrame(columns=cols)
mergedIndex = 0
for frow in dfFilter.iterrows():
    fr = frow[1]
    if fr['id'] == fr['id']: # and subjectIdMap.has_key(int(fr['id'])):
        # nan fixes
        sid = int(fr['id'])
        if fr['JAT'] == fr['JAT']:
            played = yesnodict[fr['JAT']]
        else:
            played = 0
        if fr['GYAK'] == fr['GYAK']:
            freq = playfreqdic[fr['GYAK']]
        else:
            freq = 'never'
        csvdatarr = [sid, int(fr['ELET']), fr['NEM'], schooldict[fr['ISKOLA']], handdict[fr['EEG']], fr['SERUL'], fr['PSY'], \
                     fr['CHRONIC'], fr['GYOGYSZ'], hairdict[fr['HAJ']], played, freq, yesnotrans(fr['JATTIP[JATTIP1]']), \
                     yesnotrans(fr['JATTIP[JATTIP2]']), yesnotrans(fr['JATTIP[JATTIP5]']), yesnotrans(fr['JATTIP[JATTIP3]']), \
                     yesnotrans(fr['JATTIP[JATTIP4]']), yesnotrans(fr['JATTIP[JATTIP6]']), '']
        for csv in csvFileNames:
            dfCsv = pd.read_csv(path + csv)
            for csvrow in dfCsv.iterrows():
                csvr = csvrow[1]
                if sid == csvr['KOD']:
                    if csv == 'death.csv': # 18, 19
                        csvdatarr[18] = int(csvr['MEGH']) # death
                        if csvdatarr[18] == 0:
                            csvdatarr += [0] * 8
                        else:
                            csvdatarr += [int(csvr['ERZ[E' + str(emoi) + ']']) for emoi in range(0,8)]
                    elif csv == 'day.csv':
                        csvdatarr[18] = slenderdict[csvr['SLEN']] # slender
                        csvdatarr += [int(csvr['ERZ[E' + str(emoi) + ']']) for emoi in range(0,8)]
                    else:
                        csvdatarr += [int(csvr['ERZ[E' + str(emoi) + ']']) for emoi in range(0,8)]
                    break
        
        if len(csvdatarr) == len(cols):
            merged.loc[mergedIndex] = csvdatarr
            mergedIndex += 1
        elif str(sid) in rec_dirs:
            print 'Data missing: ', sid

merged.to_csv(path + 'merged.csv')