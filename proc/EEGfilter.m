% task: load and filter all given EEG sets and save set files
subjects = [331, 347, 353, 364, 381, 384, 390, 391, 415, 421, 422, 443, 444, 450, 470, 472, 482, 487, 488, 490, 493, 498];
old_subjects = [190, 207, 213, 235, 252, 253, 269, 304, 310, 320];

SUBJECTS_EEG_TO_FILTER = old_subjects;

% load raw EEGs
for i = 1 : length(SUBJECTS_EEG_TO_FILTER)
    ALLEEG = [];
    CURRENTSET = 0;
    
    filepath = sprintf('../rec/%d/eeg/raw_eeg.edf', SUBJECTS_EEG_TO_FILTER(i));
    EEG = pop_biosig(filepath, 'channels', [3:16 36]);
    EEG = eeg_checkset( EEG );
    %EEG = pop_chanedit(EEG, 'load',{'../emotiv.ced' 'filetype' 'autodetect'});
    EEG = eeg_checkset( EEG );
    EEG.setname = num2str(SUBJECTS_EEG_TO_FILTER(i));
    EEG.filepath = filepath;
    [ALLEEG, EEG, CURRENTSET] = pop_newset(ALLEEG, EEG, 0,'gui','off');
    EEG.filepath = filepath;
    
    job = importdata('faster_job.eegjob');
    FASTER(job);
end

%for i = 1 : length(ALLEEG)
   
%    ALLEEG(i).filepath = sprintf('../rec/%d/eeg/raw_eeg.edf', subjects(i));
    
%end

% run FASTER
%job = importdata('faster_job.eegjob');
%FASTER(job);