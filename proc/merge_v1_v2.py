path = '../survey/'

fout = open(path + "merged_v1_v2.csv", "w")
# first file:
for line in open(path + "merged.csv"):
    fout.write(line)
# now the rest:
f = open(path + "merged_v1.csv")
f.next() # skip the header
for line in f:
	fout.write(line)
f.close()
fout.close()