function fea_vec = alpha_asymmetry(EEG, window_len, left_index, right_index, freqs)
	
    EEGepochs = eeg_regepochs(EEG, window_len, [-1, 1] * window_len);
    epoch_num = size(EEGepochs.data, 3);
    fea_vec = zeros(epoch_num, 1);
    
    for eid = 1 : epoch_num
        
        epoch = EEGepochs.data(:, :, eid);

        [left_psd, left_freqs] = pwelch(epoch(left_index, :), [], [], [], 128);
        [right_psd, right_freqs] = pwelch(epoch(right_index, :), [], [], [], 128);

        left_psdind = left_freqs >= freqs(1) & left_freqs <= freqs(end);
        right_psdind = right_freqs >= freqs(1) & right_freqs <= freqs(end);

        fea_vec(eid) = mean( log(left_psd(left_psdind)) - log(right_psd(right_psdind)) );
        
    end

end