% input: HR vector with NaN values
% output: interpolated HR vector
function hr_res = hr_interpol(HR, window_len)
    hr_res = HR(:, 2);
    
    % gather nonNaN data
    hrsize = size(hr_res);
    hrvals = [];
    for i = 1 : hrsize
        if (~isnan(hr_res(i)))
            hrvals = [hrvals; i, hr_res(i)];
        end
    end

    % linear interpolation
    valindex = 1;
    for i = 1:hrsize(1)
        if i > hrvals(valindex, 1) && valindex ~= size(hrvals, 1)
            valindex = valindex + 1;
        end
        % NaN val
        if (isnan(hr_res(i)))
            if valindex == 1 || valindex == size(hrvals, 1) % before first or after last sample
                hr_res(i) = hrvals(valindex, 2);
            else
                hr_res(i) = hrvals(valindex - 1, 2) + ( hrvals(valindex, 2) - hrvals(valindex - 1, 2) ) ...
                    * ( (i - hrvals(valindex - 1, 1)) / (hrvals(valindex, 1) - hrvals(valindex - 1, 1)) );
            end
        end
    end
    
    % resample
    HR(:, 2) = hr_res;
    n = window_len * 1000 / (HR(2,1) - HR(1,1));
    while mod(size(HR, 1), n) ~= 0
        HR = [HR; HR(end,:)];
    end
    hr_res = mean(reshape(HR(:,2), n, []))';

end

