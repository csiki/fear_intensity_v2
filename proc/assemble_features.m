% feature_mx: (EEG)time, situation(index), FEA, HR, GSR
function feature_mx = assemble_features(EEG, HR, settings)
    
    start_markers = [10, 20, 30, 40];
    end_markers = [11, 21, 31, 41];
    situation_lengths = [60000, 60000, 120000, 15*60*1000]; % in ms, in case of v1 subjects
    hr_marker = 1;
    v1_hr_marker = 10;
    before_end_offset = 20000; % in ms
    
    marker_times = eeg_point2lat( [ EEG.event.latency ], [], EEG.srate, ...
        [EEG.xmin, EEG.xmax] * 1000, 1E-3);
    hr_start_time = HR.data(1,1);
    hr_start_rel_eeg = marker_times(find([EEG.event.type] == hr_marker, 1));
    if (isempty(hr_start_rel_eeg)) % v1 subject with no separate gsr start marker
        hr_start_rel_eeg = marker_times(find([EEG.event.type] == v1_hr_marker, 1));
    end
    
    feature_mx = [];
    for mid = 1 : length(start_markers)
        
        if (isempty(find([EEG.event.type] == start_markers(mid), 1)))% || ... % start marker missing
                %(isempty(find([EEG.event.type] == end_markers(mid), 1)) && ... % end marker missing
                %mid ~= length(end_markers))) % but not 41
            feature_mx = [];
            warning('Missing marker #%d or #%d!', start_markers(mid));%, end_markers(mid));
            return;
        end
        
        % select EEG part
        eeg_from = marker_times(find([EEG.event.type] == start_markers(mid), end)); % in ms
        if (isempty(find([EEG.event.type] == end_markers(mid), 1)))
            eeg_to = EEG.xmax * 1000 - before_end_offset;
        elseif (isempty(find([EEG.event.type] == end_markers(mid), 1)))
            % old v1 subjects
            eeg_to = eeg_from + situation_lengths(mid);
        else
            eeg_to = marker_times(find([EEG.event.type] == end_markers(mid), end));
        end
        
        FEAselected = alpha_asymmetry(pop_select(EEG, 'time', [eeg_from, eeg_to] / 1000), ...
            settings.window_len, settings.fea_left_electrode, settings.fea_right_electrode, ...
            settings.fea_freqs);
        %FEAselected = FEAselected - mean(FEAselected);
        
        % select HR & GSR part
        hr_from = hr_start_time + eeg_from - hr_start_rel_eeg;
        hr_to = hr_start_time + eeg_to - hr_start_rel_eeg;
        hr_indices = intersect(find(HR.data(:, 1) > hr_from), find(HR.data(:, 1) < hr_to));
        HRselected = hr_interpol(HR.data(hr_indices, [1, 3]), settings.window_len); % time and HR data
        HRselected = HRselected - mean(HRselected);
        GSRselected = hr_interpol(HR.data(hr_indices, [1, 2]), settings.window_len);
        GSRselected = GSRselected - mean(GSRselected);
        
        % assemble feature matrix
        len = min([length(FEAselected), length(HRselected), length(GSRselected)]);
        times = 0 : settings.window_len : (len + 1) * settings.window_len;
        feature_mx = [feature_mx; times(1:len)', ones(len,1) * mid, ...
            FEAselected(1:len), HRselected(1:len), GSRselected(1:len)];
        
    end

end