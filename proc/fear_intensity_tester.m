% input: feature_mx - (EEG)time, situation(index), FEA, HR, GSR
% output: performace object
function perf = fear_intensity_tester(feature_mx, window_len) % kind of DEPRICATED
    
    %feature_mx = feature_mx(feature_mx(:, 2) == 4, :); % only night situation
    feature_mx = feature_mx(feature_mx(:, 2) >= 3, :); % only day and night situations
    %feature_mx(:, 3) = [0; 0; 0; feature_mx(1 : end-3, 3)];

    hr_threshold = mean(feature_mx(:, 4)) + var(feature_mx(:, 4)) / 5;
    %gsr_threshold = mean(feature_mx(:, 5)) + var(feature_mx(:, 5)) / 100;
    fea_threshold = mean(feature_mx(:, 3)) - 1 * var(feature_mx(:, 3));
    
    high_hr = feature_mx(:, 4) > hr_threshold;
    %high_gsr = feature_mx(:, 5) > gsr_threshold;
    fea_moments = feature_mx(:, 3) < fea_threshold;
    
    fea_moments_tmp = zeros(length(fea_moments), 1);
    for i = 1 : length(fea_moments)
        if (fea_moments(i) == 1)
            window_beg = max(i - window_len, 1);
            %window_beg = max(i - round(window_len / 2), 1);
            window_end = min(i + window_len, length(fea_moments));
            fea_moments_tmp(window_beg : window_end) = 1;
        end
    end
    fea_moments = fea_moments_tmp;
    tmpx = 1:length(fea_moments);
    %figure; plot([tmpx', fea_moments, high_hr]); ylim([-0.5, 1.5]);
    %figure; plot(high_hr); ylim([-0.5, 1.5]);
    %sum(fea_moments) / length(fea_moments)
    
    perf_tmp = classperf(high_hr, fea_moments, 'Positive', 1, 'Negative', 0);
    perf = [perf_tmp.Sensitivity, perf_tmp.Specificity];
    %perf_gsr = classperf(high_gsr, fea_moments)
    
    TP = sum(high_hr .* fea_moments) / sum(fea_moments);
    
    
end