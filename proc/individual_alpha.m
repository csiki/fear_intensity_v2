% input: EEG with events at least 10, 11 and 20, 21 for open- and closed-eye measurements, respectively
% output: EEG with CRB and IAF fields
function EEGout = individual_alpha(EEGin) % TODO
    
	open_eye_start = 10;
	open_eye_end = 11;
	closed_eye_start = 20;
	closed_eye_end = 21;
	
	% retrieve open and closed marker times in ms
	marker_times = eeg_point2lat( [ EEGin.event.latency ], [], EEGin.srate, [EEGin.xmin EEGin.xmax]*1000, 1E-3);
	open_start_index = find([EEGin.event.type] == open_eye_start);
	open_end_index = find([EEGin.event.type] == open_eye_end);
	closed_start_index = find([EEGin.event.type] == closed_eye_start);
	closed_end_index = find([EEGin.event.type] == closed_eye_end);
	
	open_start = marker_times(open_start_index(end)); % last occurrence of marker is needed
	open_end = marker_times(open_end_index(end));
	closed_start = marker_times(closed_start_index(end));
	closed_end = marker_times(closed_end_index(end));
	
    % load and overwrite CRB parameters
	load crbpar2;
    crbpar = crbpar2;
    %crbpar.CRBpar.spectrapar.Nfft = 256;
    %crbpar.CRBpar.timeintervals.ref_int = [closed_start + 25000, closed_end - 25000];
    %crbpar.CRBpar.timeintervals.test_int = [open_start + 25000, open_end - 25000];
    crbpar.CRBpar.timeintervals.ref_int = [closed_start, closed_end];
    crbpar.CRBpar.timeintervals.test_int = [open_start, open_end];
    crbpar.CRBpar.spectrapar.ref_winlength = 8000;
    crbpar.CRBpar.spectrapar.test_winlength = 8000;
    
    %crbpar.CRB_dataindexes = [3, 12];
    %crbpar.labels = crbpar.labels([3,12]);
    
    % update EEG
	EEGin = pop_CRBanalysis(EEGin, 1, crbpar); % fills EEG.CRB
    %EEGout.IAF = EEGout.CRB.results{4, 2};
    EEGin.IAF = mean([EEGin.CRB.results{4, 4}, EEGin.CRB.results{13, 4}]); % average of F3 and F4
    
    EEGout = EEGin;
    
end