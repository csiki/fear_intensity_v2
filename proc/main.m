% tasks:
%   - load (filtered) EEG sets, HR data, survey records
%   - assemmble feature matrices
%   - test fear intensity hyphothesis on data with corresponding high fear
%   self-report score

% TODO ! plot: fear score vs number of fea points (done it already?)

%% global settings
setting_variations; % load different settings
SETTINGS = AF3_AF4_SETTINGS; % DEF_SETTINGS
USE_ORIGINAL_EEG = 0; % if 1 loads original edf; if 0 FASTER processed EEG is loaded
CALC_FEATURES = 0;
SAVE_FEATURES = 0;
SAVE_PLOT_FRAMES = 0;

%% subject IDs
subjects = [331, 347, 353, 381, 384, 390, 391, 415, 421, 422, 443, ...
    444, 450, 470, 472, 482, 487, 488, 490, 493, 498];
subjects_w_valid_gsr = [493, 490, 415, 381, 353, 347, 331];
v1_subjects = [190, 207, 235, 252, 269, 304, 310, 320]; % 213, 253 removed (see NOTES)
subjects = [subjects, v1_subjects];
subjects_w_valid_gsr = [subjects_w_valid_gsr, v1_subjects];
subjects_w_video = [331, 347, 353, 364, 381, 384, 390, 391, 415, 421, 422, 443, 444, 450, 470, 472, 482, 487, 488, 490, 493, 498];
if SETTINGS.use_gsr_valid_subj_list
    subjects = subjects_w_valid_gsr;
end
if SAVE_PLOT_FRAMES
    subjects = subjects_w_video;
end

subjects = 482; % FIXME

%% get fear scores from survey
fid = fopen('../survey/merged_v1_v2.csv');
survey_data = struct();
survey_data.header = textscan(fid,'%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s',1, 'delimiter',',');
survey_data.data = textscan(fid,'%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s', 'delimiter',',');
oppa = [survey_data.data{2}, survey_data.data{42}];
oppa(strcmp('', oppa)) = [];
fear_scores = zeros(length(oppa) / 2, 2);
for srow = 1 : length(oppa) / 2
    fear_scores(srow, :) = [str2num(oppa{srow}), str2num(oppa{length(oppa) / 2 + srow})];
end
fclose(fid);

%% iterate over subjects - create feature matrix and infer from it
total_fea_moment_gsr_avgs = [];
total_fea_moment_gsr_after_avgs = [];
total_fea_moment_fea_values = [];
hr_avg_regulation_ratios = [];
mkdir(sprintf('fea_times/%s', SETTINGS.name)); % create fea times folder for the corresponding settings
fea_times_file = fopen(sprintf('fea_times/%s/fea_times.txt', SETTINGS.name), 'w'); % number of fea times file
root_path = '../rec/';
for sid = 1 : length(subjects)
    fprintf('Subject under processing: %d\n', subjects(sid));
    
    if (CALC_FEATURES == 1)
        if (USE_ORIGINAL_EEG == 1)
            eeg_path = strcat(root_path, num2str(subjects(sid)), '/eeg/raw_eeg.edf');
            EEG = pop_biosig(eeg_path, 'channels', [3:16 36]);
            EEG.setname = num2str(subjects(sid));
        else
            eeg_fname = strcat('ALLEEG(1)_', num2str(subjects(sid)), '.set.set');
            EEG = pop_loadset('filename', eeg_fname, 'filepath', 'faster_res/');
        end

        EEG = pop_chanedit(EEG, 'load',{'../emotiv.ced' 'filetype' 'autodetect'});

        hr_path = strcat(root_path, num2str(subjects(sid)), '/gsr/raw_gsr.txt');
        HR = importdata(hr_path);

        % assemble feature matrix and save to file
        feature_mx = assemble_features(EEG, HR, SETTINGS);

        if (SAVE_FEATURES == 1)
            if (USE_ORIGINAL_EEG == 1)
                saveto = sprintf('features/%s/feature_mx_%d.mat', SETTINGS.name, subjects(sid));
            else
                saveto = sprintf('features/%s/feature_mx_preproc_%d.mat', SETTINGS.name, subjects(sid));
            end
            save(saveto, 'feature_mx');
        end
    else
        if (USE_ORIGINAL_EEG == 1)
            feature_mx = importdata(sprintf('features/%s/feature_mx_%d.mat', SETTINGS.name, subjects(sid)));
        else
            feature_mx = importdata(sprintf('features/%s/feature_mx_preproc_%d.mat', SETTINGS.name, subjects(sid)));
        end
    end
    
    % plot FEA, HR, GSR
%     figure; plot(feature_mx(:, [3, 4, 5]));
%     title(subjects(sid));
%     xlabel(strcat('Time (', int2str(SETTINGS.window_len), ' sec)'));
%     ylabel('Normalized FEA, HR, GSR');
%     legend('FEA', 'HR', 'GSR');
    
    % make fea times
    open_or_day_rowindex = feature_mx(:, 2) == 1 | feature_mx(:, 2) == 3;
    fea_threshold = mean(feature_mx(open_or_day_rowindex, 3)) - SETTINGS.fea_variance_threshold_multiplier * var(feature_mx(open_or_day_rowindex, 3));
    night_features_mx = feature_mx(find(feature_mx(:, 1) == 0, 1, 'last'):end, :);
    fea_moments = night_features_mx(:, 3) < fea_threshold;
    fea_moment_times = night_features_mx(fea_moments, 1);
    total_fea_moment_fea_values = [total_fea_moment_fea_values; night_features_mx(fea_moments, 3)];
    saveto = sprintf('fea_times/%s/fea_times_%d.txt', SETTINGS.name, subjects(sid));
    fileID = fopen(saveto, 'w');
    fprintf(fileID, '%f\n', fea_moment_times);
    fclose(fileID);
    fprintf(fea_times_file, '%d\t%d\n', subjects(sid), length(fea_moment_times));
            
    % calculate [mean HR values around FEA drops] - [mean HR values after FEA drops]
    % (the above calculation is not robust enough)
    fea_moment_gsr_avgs = zeros(length(fea_moment_times), SETTINGS.regulation_avg_hr_gsr_win_len / SETTINGS.window_len);
    after_fea_moment_gsr_avgs = zeros(length(fea_moment_times), SETTINGS.regulation_avg_hr_gsr_win_len / SETTINGS.window_len);
    for feaindex = 1 : length(fea_moment_times)
        if night_features_mx(end, 1) >= fea_moment_times(feaindex) + SETTINGS.regulation_hr_gsr_reaction_time + SETTINGS.regulation_window_len + SETTINGS.regulation_avg_hr_gsr_win_len / 2
            
            % plot environment of FEA phenomenon
            fea_environment = night_features_mx( ...
                night_features_mx(:, 1) >= (fea_moment_times(feaindex) + SETTINGS.regulation_hr_gsr_reaction_time - 10) & ...
                night_features_mx(:, 1) < (fea_moment_times(feaindex) + SETTINGS.regulation_hr_gsr_reaction_time + SETTINGS.regulation_window_len + 10), :);
            figure; plot(fea_environment(:, 1), fea_environment(:, 3:end));
            title(strcat(num2str(subjects(sid)), '; FEA time: ', num2str(fea_moment_times(feaindex))));
            xlabel(strcat('Time (sec)'));
            ylabel('HR (green), FEA (blue), GSR (red) values (baseline removed)')
            
            moment_beg = fea_moment_times(feaindex) + SETTINGS.regulation_hr_gsr_reaction_time - SETTINGS.regulation_avg_hr_gsr_win_len / 2;
            moment_end = fea_moment_times(feaindex) + SETTINGS.regulation_hr_gsr_reaction_time + SETTINGS.regulation_avg_hr_gsr_win_len / 2;
            retrieved_vital_len = length(night_features_mx( ...
                night_features_mx(:, 1) >= moment_beg & ...
                night_features_mx(:, 1) < moment_end, SETTINGS.hr_or_gsr_regulation_feature_index));
            if retrieved_vital_len < SETTINGS.regulation_avg_hr_gsr_win_len / SETTINGS.window_len
                % if there's not enough vital recorded, e.g. at the end of
                % recordings, just skip this fea moment
                continue; % fea_moment_gsr_avgs inialized 0 anyway
            end
            fea_moment_gsr_avgs(feaindex, :) = night_features_mx( ...
                night_features_mx(:, 1) >= moment_beg & ...
                night_features_mx(:, 1) < moment_end, SETTINGS.hr_or_gsr_regulation_feature_index);
            
            after_moment_beg = fea_moment_times(feaindex) + SETTINGS.regulation_hr_gsr_reaction_time + SETTINGS.regulation_window_len - SETTINGS.regulation_avg_hr_gsr_win_len / 2;
            after_moment_end = fea_moment_times(feaindex) + SETTINGS.regulation_hr_gsr_reaction_time + SETTINGS.regulation_window_len + SETTINGS.regulation_avg_hr_gsr_win_len / 2;
            after_fea_moment_gsr_avgs(feaindex, :) = night_features_mx( ...
                night_features_mx(:, 1) >= after_moment_beg & ...
                night_features_mx(:, 1) < after_moment_end, SETTINGS.hr_or_gsr_regulation_feature_index);
        end
    end
    total_fea_moment_gsr_avgs = [total_fea_moment_gsr_avgs; fea_moment_gsr_avgs];
    total_fea_moment_gsr_after_avgs = [total_fea_moment_gsr_after_avgs; after_fea_moment_gsr_avgs];
    avg_hr_changes = mean(fea_moment_gsr_avgs, 2) - mean(after_fea_moment_gsr_avgs, 2);
    hr_avg_regulation_ratios = [hr_avg_regulation_ratios; sum(avg_hr_changes > 0) / length(avg_hr_changes)];
    
    %night_features_mx( ...
    %    night_features_mx(:,1) > (fea_moment_times - SETTINGS.regulation_avg_hr_gsr_win_len) ...
    %    && night_features_mx(:,1) < (fea_moment_times + SETTINGS.regulation_avg_hr_gsr_win_len), [1, 3, 4])
    
    % filter ratyi GSR
    %plot(feature_mx(:,1), feature_mx(:,5));
    
    % make video frames out of FEA, HR, GSR data
    if SAVE_PLOT_FRAMES
        gen_plot_movie_frames(subjects(sid), night_features_mx);
    end
    
end
fclose(fea_times_file);

%% before-after fea moment difference analysis
[h, p, ci, stats] = ttest(mean(total_fea_moment_gsr_avgs, 2), mean(total_fea_moment_gsr_after_avgs, 2))
fea_moment_gsr_difference = mean(total_fea_moment_gsr_avgs, 2) - mean(total_fea_moment_gsr_after_avgs, 2);
figure;
hist(fea_moment_gsr_difference, 50);
title('GSR difference between before and after fea moment');

figure;
plot(total_fea_moment_fea_values, mean(total_fea_moment_gsr_avgs, 2) - mean(total_fea_moment_gsr_after_avgs, 2), '.');
hold on;
linfit = polyfit(total_fea_moment_fea_values, mean(total_fea_moment_gsr_avgs, 2) - mean(total_fea_moment_gsr_after_avgs, 2), 1);
xtrend = min(total_fea_moment_fea_values):0.001:max(total_fea_moment_fea_values);
plot(xtrend, polyval(linfit, xtrend), 'r-');