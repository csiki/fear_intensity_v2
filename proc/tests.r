# load
setwd("c:/CSIKI/BCI/fear_intensity_v2/proc/")
fea_times_df <- read.table("fea_times/fea_times.txt", header = F, sep = '\t', col.names = c("id", "fea_times"))
survey_data <- read.csv("../survey/merged_v1_v2.csv")

# clean
summary(survey_data)
survey_data$played <- factor(survey_data$played)
survey_data$play_fps <- factor(survey_data$play_fps)
survey_data$play_rts <- factor(survey_data$play_rts)
survey_data$play_horror <- factor(survey_data$play_horror)
survey_data$play_sport <- factor(survey_data$play_sport)
survey_data$play_simulation <- factor(survey_data$play_simulation)
survey_data$play_rpg <- factor(survey_data$play_rpg)
survey_data2 <- merge(survey_data, fea_times_df)
attach(survey_data2)

# fear score significance at night (repeated measures t-tests: night vs day, day vs base, night vs base)
shapiro.test(base_fear) # p < .05, thus it is not normally distributed
shapiro.test(day_fear) # p < .05, thus it is not normally distributed
shapiro.test(night_fear) # p > .05, thus it looks like normally distributed
# base_fear and day_fear not normally distributed, but their mean is more likely
hist((base_fear + day_fear) / 2)
shapiro.test((base_fear + day_fear) / 2) # yep
t.test(base_fear, day_fear, paired = T) # p > = .20, thus difference is not significant
t.test(base_fear, night_fear, paired = T) # t(12) = 7.0108, p < .01, thus difference is significant
t.test(day_fear, night_fear, paired = T) # t(12) = 5.3295, p < .01, thus difference is significant
# the last three tests cannot be taken seriously, as base and day scores are not significantly normal distributed
t.test((base_fear + day_fear) / 2, night_fear, paired = T) # t(12) = 6.4968, p < .01, thus difference is significant

# fea peak times X fear rate after night game --> no sifnigicance
plot(night_fear - day_fear, fea_times)
abline(lm(fea_times ~ (night_fear - day_fear)))

iqs <- rnorm(10000, 100, 15)
sum(iqs > 121) / length(iqs)
hist(iqs)

qnorm(0.5, mean = 100, sd = 15)