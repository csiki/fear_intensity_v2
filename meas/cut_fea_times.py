
from config import *
import subprocess
import os
import string
import easygui
import numpy as np
import shutil

settings_name = easygui.enterbox(msg="Give the name of the settings, for which the fea videos should be calculated", title="Settings name")
fea_times_path = "../proc/fea_times/" + settings_name + "/"
cut_len = 10

vidout_base_path = "../show/fea_times_video/" + settings_name + "/"
if os.path.exists(vidout_base_path): # delete recursively if exists
    shutil.rmtree(vidout_base_path)
os.mkdir(vidout_base_path)
for fea_time_file_id in os.listdir(fea_times_path):

    if fea_time_file_id == 'fea_times.txt':
        continue # skip

    subjid = fea_time_file_id[-7:-4]
    videoin = "../show/video/" + subjid + ".mp4"
    #video_keyframed = "../show/video/" + subjid + "_keys.mp4"

    if os.stat(fea_times_path + fea_time_file_id).st_size > 1 \
            and os.path.isfile(fea_times_path + fea_time_file_id):
        with open(fea_times_path + fea_time_file_id) as fea_time_file:
            fea_times = map(float, fea_time_file)

        # insert keyframes
        start_times = np.array(fea_times) - cut_len / 2.0
        end_times = np.array(fea_times) + cut_len / 2.0
        start_times = [str(x) for x in start_times.tolist()]
        end_times = [str(x) for x in end_times.tolist()]
        key_frame_times = ','.join(start_times)
        #proc = subprocess.Popen(ffmpeg_path + " -i " + videoin + " -force_key_frames " + key_frame_times + " -segment_times " + key_frame_times + " -segment_time_delta 0.05 " + video_keyframed, shell=True)
        #proc.wait()

        # remove / average close fea_times
        filtered_fea_times = []
        tmp_close_points = np.array([])
        for fea_time in fea_times:
            if len(tmp_close_points) > 0:
                if fea_time - tmp_close_points[0] < cut_len / 2: # close time points
                    tmp_close_points = np.append(tmp_close_points, fea_time)
                else: # add to filtered the mean of the prev, and init tmp_close_points with the new "far" value
                    filtered_fea_times.append(tmp_close_points.mean())
                    tmp_close_points = np.array([fea_time])
            else:
                tmp_close_points = np.append(tmp_close_points, fea_time)

        # cut videos
        counter = 0
        for fea_time in fea_times:
            videoout = vidout_base_path + subjid + "_" + str(counter) + ".mp4"
            proc = subprocess.Popen(ffmpeg_path + " -i " + videoin + " -ss " + str(fea_time - cut_len / 2) + " -c:v libx264 -c:a aac -strict experimental -t " + str(cut_len) + " " + videoout, shell=True)
            proc.wait()
            counter = counter + 1

