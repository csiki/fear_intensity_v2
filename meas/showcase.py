import easygui
from config import *
import os

#ID = easygui.integerbox(msg="ID number", title="ID", upperbound=10000, lowerbound=0)

subjects = [331, 347, 353, 364, 381, 384, 390, 391, 415, 421, 422, 443, 444, 450, 470, 472, 482, 487, 488, 490, 493, 498]
subjects_w_valid_gsr = [493, 490, 415, 381, 353, 347, 331]
video_lengths = {331: 179, 347: 900, 353: 440, 364: 900, 381: 888, 384: 900, 390: 900, 391: 312, 415: 0, 421: 900, 422: 541, 443: 653,
                 444: 899, 450: 795, 470: 341, 472: 748, 482: 647, 487: 514, 488: 900, 490: 900, 493: 900, 498: 710}; # in sec
static_bg = "../show/video/video_bg.jpg"
plot_framerate = 4

for ID in subjects:
    base_rec_dir = "../rec/" + str(ID) + "/"
    show_video_dir = "../show/video/"
    plot_video_dir = "../show/video/plot/"

    left_video = base_rec_dir + screen_dir + "night.mp4"
    right_video = base_rec_dir + webcam_dir + "night.mp4"
    out_video = show_video_dir + str(ID) + ".mp4"
    out_w_plot_video = show_video_dir + str(ID) + "_w_plot.mp4"
    out_w_plot_video_compr = show_video_dir + str(ID) + "_w_plot_compr.mp4"
    plot_frames_path = "../show/plot_frames/" + str(ID) + "/"
    fea_frames = plot_frames_path + "fea%d.png"
    hr_frames = plot_frames_path + "hr%d.png"
    gsr_frames = plot_frames_path + "gsr%d.png"

    plot_video_fea = plot_video_dir + str(ID) + "fea.mov"
    plot_video_hr  = plot_video_dir + str(ID) + "hr.mov"
    plot_video_gsr = plot_video_dir + str(ID) + "gsr.mov"

    if not os.path.isfile(left_video) and not os.path.isfile(right_video):
        continue # skip if no screen video available
    if not os.path.isfile(out_video):
        if os.path.isfile(left_video) and os.path.isfile(right_video): # both video available
            proc = subprocess.Popen(ffmpeg_path + " -y -i " + left_video + " -itsoffset 1 -i " + right_video + " -filter_complex \"[0:v] scale=iw/2:ih/2, pad=1.7*iw:ih, crop=6/7*iw:ih [left]; [1:v] scale=iw:ih [right]; [left][right] overlay=main_w/1.85:0 [out]\" -t " + str(video_lengths[ID]) + " -b:v 768k -map 0:a -map 1:a -map \"[out]\" " + out_video, shell=True)
        elif not os.path.isfile(right_video): # only screen video available
            proc = subprocess.Popen(ffmpeg_path + " -y -loop 1 -i " + static_bg + " -i " + left_video + " -filter_complex \"[1:v] scale=iw/2:ih/2, pad=1.7*iw:ih, crop=6/7*iw:ih [left]; [0:v][left] overlay=0:0 [out]\" -b:v 768k -map 1:a -c:a copy -map \"[out]\" -t " + str(video_lengths[ID]) + " " + out_video, shell=True)
        else: # only webcam video available
            proc = subprocess.Popen(ffmpeg_path + " -y -loop 1 -i " + static_bg + " -itsoffset 1 -i " + right_video + " -filter_complex \"[1:v] scale=iw:ih, pad=1.7*iw:ih [left]; [0:v][left] overlay=0:0 [out]\" -b:v 768k -map 1:a -c:a copy -map \"[out]\" -t " + str(video_lengths[ID]) + " " + out_video, shell=True)
        proc.wait()

    if not os.path.isfile(out_w_plot_video):
        # gen plot videos
        proc = subprocess.Popen(ffmpeg_path + " -y -framerate " + str(plot_framerate) + " -itsoffset 1 -i " + fea_frames + " -r 30 -vcodec png " + plot_video_fea)
        proc.wait()
        proc = subprocess.Popen(ffmpeg_path + " -y -framerate " + str(plot_framerate) + " -itsoffset 1 -i " + hr_frames + " -r 30 -vcodec png " + plot_video_hr)
        proc.wait()
        proc = subprocess.Popen(ffmpeg_path + " -y -framerate " + str(plot_framerate) + " -itsoffset 1 -i " + gsr_frames + " -r 30 -vcodec png " + plot_video_gsr)
        proc.wait()
        # mashup videos
        proc = subprocess.Popen(ffmpeg_path + " -y -i " + out_video + " -i " + plot_video_fea +
                                " -filter_complex \"[1:v]scale=227:181 [ovrl],[0:v][ovrl]overlay=main_w/2+55:5\" -t " + str(video_lengths[ID]) + " " + out_w_plot_video) # 227x181
        proc.wait();
        proc = subprocess.Popen(ffmpeg_path + " -y -i " + out_w_plot_video + " -i " + plot_video_hr +
                                " -filter_complex \"[1:v]scale=227:181 [ovrl],[0:v][ovrl]overlay=main_w/2+55:main_h/2+85\" -t " + str(video_lengths[ID]) + " " + out_w_plot_video+"tmp.mov") # 227x181
        proc.wait();
        if ID in subjects_w_valid_gsr:
            proc = subprocess.Popen(ffmpeg_path + " -y -i " + out_w_plot_video+"tmp.mov" + " -i " + plot_video_gsr +
                                    " -filter_complex \"[1:v]scale=227:181 [ovrl],[0:v][ovrl]overlay=main_w-232:main_h/2+85\" -t " + str(video_lengths[ID]) + " " + out_w_plot_video) # 227x181
            proc.wait();
            os.remove(out_w_plot_video+"tmp.mov")
        else:
            os.remove(out_w_plot_video)
            os.rename(out_w_plot_video+"tmp.mov", out_w_plot_video)  # no gsr added, just change file name back from tmp

    # if not os.path.isfile(out_w_plot_video_compr):
    #     proc = subprocess.Popen(ffmpeg_path + " -y -i " + out_w_plot_video + " -preset " + reencode_speed + " " + out_w_plot_video_compr)
    #     proc.wait()

# ffmpeg.exe -i screen/night.mp4 -i webcam/night.mp4 -filter_complex "[0:v] scale=iw/2:ih/2, pad=1.7*iw:ih, crop=6/7*iw:ih [left]; [1:v] scale=iw:ih [right]; [left][right] overlay=main_w/1.85:0 [out]" -b:v 768k -map 0:a -map 1:a -map "[out]" Output2.mp4