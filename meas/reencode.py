import easygui
from config import *
import os

#ID = easygui.integerbox(msg="ID number", title="ID", upperbound=10000, lowerbound=0)

for base_rec_dir in os.listdir("../rec/"):
    #base_rec_dir = "../rec/" + str(ID) + "/"
    base_rec_dir = "../rec/" + base_rec_dir + "/"
    #print base_rec_dir
    if os.path.isfile(base_rec_dir + screen_dir + "temp_day.mp4"):
        try:
            # re-encode videos
            proc1 = subprocess.Popen("ffmpeg.exe -i " + base_rec_dir + screen_dir + "temp_day.mp4 -preset " + reencode_speed + " " + base_rec_dir + screen_dir + "day.mp4")
            proc2 = subprocess.Popen("ffmpeg.exe -i " + base_rec_dir + screen_dir + "temp_night.mp4 -preset " + reencode_speed + " " + base_rec_dir + screen_dir + "night.mp4")
            proc3 = subprocess.Popen("ffmpeg.exe -i " + base_rec_dir + webcam_dir + "temp_day.mp4 -preset " + reencode_speed + " " + base_rec_dir + webcam_dir + "day.mp4")
            proc4 = subprocess.Popen("ffmpeg.exe -i " + base_rec_dir + webcam_dir + "temp_night.mp4 -preset " + reencode_speed + " "  + base_rec_dir + webcam_dir + "night.mp4")
            proc1.wait()
            proc2.wait()
            proc3.wait()
            proc4.wait()

            # delete previous videos
            os.remove(base_rec_dir + screen_dir + "temp_day.mp4")
            os.remove(base_rec_dir + screen_dir + "temp_night.mp4")
            os.remove(base_rec_dir + webcam_dir + "temp_day.mp4")
            os.remove(base_rec_dir + webcam_dir + "temp_night.mp4")
            print base_rec_dir
        except:
            print "Para @ " + base_rec_dir

# adjust sound of webcam recordings
"""
proc1 = subprocess.Popen("ffmpeg.exe -i " + base_rec_dir + webcam_dir + "day.mp4 -itsoffset " + str(webcam_audio_offset) \
    + " -i " + base_rec_dir + webcam_dir + "day.mp4 -codec copy -map 0:0 -map 1:1 " + base_rec_dir + webcam_dir + "day_fixed.mp4")
proc2 = subprocess.Popen("ffmpeg.exe -i " + base_rec_dir + webcam_dir + "night.mp4 -itsoffset " + str(webcam_audio_offset) \
    + " -i " + base_rec_dir + webcam_dir + "night.mp4 -codec copy -map 0:0 -map 1:1 " + base_rec_dir + webcam_dir + "night_fixed.mp4")
proc1.wait()
proc2.wait()
"""