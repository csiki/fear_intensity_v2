import subprocess
import msvcrt, signal
import time

# MEAS STUFF
eeg_dir = "eeg/"
gsr_dir = "gsr/"
screen_dir = "screen/"
webcam_dir = "webcam/"

path_to_slender = "c:/CSIKI/GAMES/Slender v0.9.7/"
slender_day_name = "Slender_day.exe"
slender_night_name = "Slender_night.exe"

ffmpeg_path = "c:/ffmpeg/bin/ffmpeg.exe"
serial_port_name = "COM1"

master_vol = 0.3

GSR_HR_marker = 1
open_start_marker = 10
open_end_marker = 11
close_start_marker = 20
close_end_marker = 21
day_start_marker = 30
day_end_marker = 31
night_start_marker = 40
night_end_marker = 41

open_prep_duration = 10
open_meas_duration = 60
close_prep_duration = 10
close_meas_duration = 60
day_prep_duration = 30
day_meas_duration = 120
night_prep_duration = 30
night_meas_duration = 900
"""
open_prep_duration = 1
open_meas_duration = 1
close_prep_duration = 1
close_meas_duration = 1
day_prep_duration = 2
day_meas_duration = 5
night_prep_duration = 20
night_meas_duration = 30
"""
chrome_path = "c:/Program Files (x86)/Google/Chrome/Application/chrome.exe"
before_game_survey = "http://agykutatas.co.nf/index.php/254575/lang-hu"
after_daygame_survey = "http://agykutatas.co.nf/index.php/348319/lang-hu"
after_nightgame_survey = "http://agykutatas.co.nf/index.php/517343/lang-hu"

# RE-ENCODE STUFF
reencode_speed = "slow"
webcam_audio_offset = -1.5

# HELPER FUNCTIONS
def set_master_vol(to):
    subprocess.Popen("nircmd.exe setsysvolume " + str(round(to * 65535)))
def set_app_vol(app, to):
    subprocess.Popen("nircmd.exe setappvolume " + app + " " + str(to))

def do_nothing(): pass
def send_marker(ser_conn, marker): ser_conn.write(chr(marker))
def stop_if_esc_pressed(timer):
    while True:
        if not timer.isAlive():
            break
        if msvcrt.kbhit() and msvcrt.getch() == chr(27).encode():
            timer.function(*timer.args)
            timer.cancel()
            print "Timer stopped"
            break
        time.sleep(0.5)
def send_marker_kill_recordings(ser_conn, marker, screen_recorder, webcam_recorder):
    ser_conn.write(chr(marker))
    if screen_recorder.poll() == None:
        screen_recorder.send_signal(signal.CTRL_C_EVENT)
    if webcam_recorder.poll() == None:
        webcam_recorder.send_signal(signal.CTRL_C_EVENT)