#!/usr/bin/env python
# -*- coding: utf-8 -*-

import serial
import window_mgr
import easygui
import os
import sched
import threading
from config import *

"""
Course of measurement:
    - ask for id and create folders accordingly
    - open Slender_day, Slender_night (ask if started properly, so volume can be set)
    - set master, Slender_day, Slender_night volume (mid, 0, 0)
    - change window to Slender_day to start round (manual back)
    - change window to Slender_night to start round (manual back)
    - switch to before-game survey and back
    - checklist before start
        - EEG, GSR, HR, earphone on
        - lights off, curtains (hangulatvilagitas)
        - 2 games started and paused
    - GSR, HR sync (1)
    - general instructions
    - open-eye instructions
    - open-eye measurement
        - timer start
        - EEG marker (10)
        - timer timeout
        - EEG marker (11)
    - closed-eye instructions
    - closed-eye measurement
        - timer start
        - EEG marker (20)
        - timer timeout
        - EEG marker (21)
    - day game instructions
    - day game measurement
        - adjust volume
        - timer start
        - EEG marker (30)
        - screen recorder start and sync
        - change window to Slender_day, send ESC keystroke to unpause
        - timer timeout
        - EEG marker (31)
        - terminate Slender, change window back
    - switch to after-daygame survey and back
    - night game instructions
    - night game measurement
        - adjust volume
        - timer start
        - EEG marker (40)
        - screen recorder start and sync
        - change window to Slender_night, send ESC keystroke to unpause
        - [in case of dying] listen to ESC keystroke, initiate timeout if pressed
        - timer timeout
        - EEG marker (41)
        - terminate Slender, change window back
    - switch to after-nightgame survey and back
"""

# ask for id and create folders accordingly
ID = easygui.integerbox(msg="Add meg az azonosítószámod!", title="Azonosítószám", upperbound=10000, lowerbound=0)
base_rec_dir = "../rec/" + str(ID) + "/"

if not os.path.exists(base_rec_dir):
    os.makedirs(base_rec_dir)
    os.makedirs(base_rec_dir + eeg_dir)
    os.makedirs(base_rec_dir + gsr_dir)
    os.makedirs(base_rec_dir + screen_dir)
    os.makedirs(base_rec_dir + webcam_dir)

# open Slender_day, Slender_night
slender_night_proc = subprocess.Popen(path_to_slender + slender_night_name)
slender_day_proc = subprocess.Popen(path_to_slender + slender_day_name)

# (ask if started properly, so volume can be set)
easygui.msgbox("Are the games started properly so volume can be adjusted?")

# set master, Slender_day, Slender_night volume (mid, 0, 0)
set_master_vol(master_vol)
set_app_vol(slender_day_name, 0)
set_app_vol(slender_night_name, 0)

# change window to Slender_day and Slender_night to start round (manual back)
easygui.msgbox("Are both games started and paused?")

# switch to before-game survey and back
subprocess.Popen(chrome_path + " " + before_game_survey + " -incognito")
easygui.msgbox(msg="Ha végeztél a kérdőívvel, kattints az OK gombra!", title="Kérdőív befejezése", ok_button="OK")

# calibrate webcam
screen_recorder = subprocess.Popen(ffmpeg_path + " -f dshow -r 20 -i video=\"USB2.0 HD UVC WebCam\" -s 1280x960 -f sdl \"Webcam calibration\"")
easygui.msgbox("Is webcam calibration finished?")
screen_recorder.kill()

# checklist before start
easygui.msgbox("- EEG, GSR, HR, earphone on\n- lights off, curtains\n- 2 games started and paused\n- internet connection available",\
               ok_button="Done", title="Checklist")

# initiate serial port
successful_serial_conn = False
ser = None
while not successful_serial_conn:
    try:
        ser = serial.Serial(serial_port_name)
        successful_serial_conn = True
    except serial.SerialException as e:
        successful_serial_conn = False
        easygui.msgbox("Could not connect to serial port " + serial_port_name + "!\nIn detail: " + str(e), title="Error");

# GSR, HR sync (1)
easygui.msgbox("Ready to sync GSR?", title="GSR sync", ok_button="Sync")
ser.write(chr(GSR_HR_marker))

# general instructions
easygui.msgbox(ok_button="OK", title="Általános instrukciók", msg="A következõ kb. félórában egy videojátékkal "
            "fogsz játszani, illetve bizonyos esetekben arra fogunk kérni, hogy hajts végre néhány feladatot. "
            "Az instrukciókat a képernyõn fogod látni.\n"
            "Helyezkedj el kényelmesen, és a következõ félórában ne törõdj semmi mással a vizsgálaton kívül! "
            "A méréseket megzavarhatja a mozgás, különösen az állkapocs és az arcizmok mozgása. Kérjük, hogy miközben "
            "játszol, vagy feladatot hajtasz végre, próbálj meg minél kevesebbet mozogni, és ne beszélj! "
            "Amikor kérdõívet töltesz ki, vagy az instrukciókat látod a képernyõn, nyugodtan mozoghatsz.\n"
            "Az instruciók elolvasása után a OK megnyomásával tudod elkezdeni a meghatározott feladatot.\n"
            "Ha bármilyen kérdésed van fordulj nyugodtan a kutatásvezetõhöz most!")

# open-eye instructions
easygui.msgbox(ok_button="OK", title="Nyitott szemes mérés", msg="Most az lesz a feladatod, hogy nyitott szemmel ülj "
    "nyugodtan 1 percig, és lehetõleg ne mozogj vagy beszélj közben! Ha készen állsz, nyomj az OK gombra és azonnal elkezdődik a mérés!")

# open-eye measurement
timer = sched.scheduler(time.time, time.sleep)
timer.enter(open_prep_duration, 1, do_nothing, ())
timer.run()
timer.enter(open_meas_duration, 1, send_marker, (ser, open_end_marker))
ser.write(chr(open_start_marker))
timer.run()

# closed-eye instructions
easygui.msgbox(ok_button="OK", title="Csukott szemes mérés", msg="Most az lesz a feladatod, hogy csukott szemmel ülj"
        "nyugodtan 1 percig, lehetõleg ne mozogj vagy beszélj közben!"
        "Amikor lejár az egy perc, a vizsgálatvezetõ szólni fog. A kezdéshez nyomj az OK gombra!")

# closed-eye measurement
timer = sched.scheduler(time.time, time.sleep)
timer.enter(close_prep_duration, 1, do_nothing, ())
timer.run()
timer.enter(close_meas_duration, 1, send_marker, (ser, close_end_marker))
ser.write(chr(close_start_marker))
timer.run()

# day game instructions
easygui.msgbox(ok_button="OK", title="Felfedező játék instrukciók", msg="Most egy belsõ nézetes játékkal fogsz játszani. "
    "A WASD billentyûk és az egér segítségével tudsz mozogni.A futáshoz nyomd le a shiftet. Az egyedüli feladatod most "
    "a játékterület felfedezése lesz. Ha egy papírfecnit találnál, azt kérlek hagyd figyelmen kívül. Ha bármilyen "
    "kérdésed lenne fordulj nyugodtan a kísérletvezetõhöz. Egyébként nyomj az OK gombra, majd válts az 1-es ikonú játékra (tálcán találod)!")

# day game measurement
set_app_vol(slender_day_name, 1)
timer = sched.scheduler(time.time, time.sleep)
timer.enter(day_prep_duration, 1, do_nothing, ())
timer.run()
subprocess.Popen(ffmpeg_path + " -f dshow -i audio=\"virtual-audio-capturer\":video=\"screen-capture-recorder\""
                 " -rtbufsize 1500M -preset ultrafast -t " + str(day_meas_duration) + " " + base_rec_dir + screen_dir + "temp_day.mp4", shell=True)
subprocess.Popen(ffmpeg_path + " -f dshow -i video=\"USB2.0 HD UVC WebCam\":audio=\"Microphone (Realtek High Definition Audio)\""
                 " -rtbufsize 1500M -preset ultrafast -t " + str(day_meas_duration) + " " + base_rec_dir + webcam_dir + "temp_day.mp4", shell=True)
timer.enter(day_meas_duration, 1, send_marker, (ser, day_end_marker))
ser.write(chr(day_start_marker))
timer.run()
os.system("taskkill /im " + slender_day_name)
easygui.msgbox(ok_button="OK", title="Game over", msg="Első játék vége! Nyomj az OK gombra!")
w = window_mgr.WindowMgr()
w.find_window_wildcard(".*Game.*")
w.set_foreground()

# switch to after-daygame survey and back
subprocess.Popen(chrome_path + " " + after_daygame_survey + " -incognito")

# night game instructions
easygui.msgbox(ok_button="OK", title="Éjszakai játék instrukciók", msg="Ugyanazzal a játékkal fogsz játszani, csak sötétben. "
    "Most papírfecnik összegyûjtése lesz a feladatod (összesen nyolcat kell összegyûjtened) —  a fecniket az egér bal "
    "gombjának lenyomásával tudod felszedni. A játékot sikeresen végigviheted ha mindet megtalálod. Lesz valaki, aki ebben "
    "megpróbál megakadályozni. Tudni fogod mikor van a közeledben, ekkor próbálj minél messzebb kerülni tõle és a legfontosabb: ne nézz rá! "
    "A papírokon lévõ üzenetek adhatnak további tippeket. Ha bármilyen kérdésed lenne fordulj nyugodtan a kísérletvezetõhöz. "
    "Egyébként nyomj az OK gombra, majd válts az 2-es ikonú játékra (tálcán találod)!")

# night game measurement
set_app_vol(slender_night_name, 1)
timer = sched.scheduler(time.time, time.sleep)
timer.enter(night_prep_duration, 1, do_nothing, ())
timer.run()
screen_rec = subprocess.Popen(ffmpeg_path + " -f dshow -i audio=\"virtual-audio-capturer\":video=\"screen-capture-recorder\""
                 " -rtbufsize 1500M -preset ultrafast -t " + str(night_meas_duration) + " " + base_rec_dir + screen_dir + "temp_night.mp4")
webcam_rec = subprocess.Popen(ffmpeg_path + " -f dshow -i video=\"USB2.0 HD UVC WebCam\":audio=\"Microphone (Realtek High Definition Audio)\""
                 " -rtbufsize 1500M -preset ultrafast -t " + str(night_meas_duration) + " " + base_rec_dir + webcam_dir + "temp_night.mp4")
#timer = threading.Timer(night_meas_duration, send_marker_kill_recordings, (ser, night_end_marker, screen_recorder, webcam_recorder))
timer = threading.Timer(night_meas_duration, send_marker, (ser, night_end_marker))
ser.write(chr(night_start_marker))
timer.start()
stop_if_esc_pressed(timer)

# start after night game survey
subprocess.Popen(chrome_path + " " + after_nightgame_survey + " -incognito")

# kill leftover processes
screen_rec.send_signal(signal.CTRL_C_EVENT)
webcam_rec.send_signal(signal.CTRL_C_EVENT)
os.system("taskkill /im " + slender_night_name)

# close serial port
ser.close()
exit(0)